require "nvchad.mappings"
local map = vim.keymap.set

map("n", ";", ":", { desc = "CMD enter command mode" })

map("n", "<leader>tt", function()
  require("base46").toggle_transparency()
end, { desc = "Toggle termianl transparency" })

map("n", "<leader>qq", function()
  require("nvchad_ui.tabufline").closeAllBufs()
end, { desc = "Close all buffers" })

map("n", "<leader>dd", ":lua vim.diagnostic.disable()<CR>", { desc = "LSP Disable diagnostic" })
map("n", "<leader>md", ":Glow<CR>", { desc = "MARKDOWN-VIEWER Start" })
map("n", "<leader>co", ":Copilot enable<CR>", { desc = "COPILOT enable" })

map("n", "<leader>mt", function()
  require("mini.map").toggle()
end, { desc = "MINI-MAP Toggle" })

map("n", "<leader>mf", function()
  require("mini.map").toggle_focus()
end, { desc = "MINI-MAP Focus" })
